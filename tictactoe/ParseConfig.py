__author__ = 'Justyna'

from BeautifulSoup import BeautifulSoup

class ParseConfig():

    def parse(self):
        with open("config.xml") as f:
            content = f.read()

        y = BeautifulSoup(content)
        self.player = y.player.contents[0]

    def getPlayer(self):
        return self.player