from ParseConfig import ParseConfig
from State import State

__author__ = 'Justyna'

class TicTacToe():


    mainState = [0,1,2,
                 3,4,5,
                 6,7,8]
    state = [0,0,0,
             0,0,0,
             0,0,0]
    copyState = ['_','_','_',
             '_','_','_',
             '_','_','_']

    def __init__(self):
        self.stateObj = State(self.state, self.mainState, self.copyState)
        self.stateObj.displayMainState()
        self.stateObj.displayState()

        self.config = ParseConfig()
        self.config.parse()

        if self.config.getPlayer() == 'X':
            self.player = 1
        else:
            self.player = 2

    def checkWin(self, state, player):
        if state[0]==player and state[1]==player and state[2]==player:
            return 'win'
        if state[3]==player and state[4]==player and state[5]==player:
            return 'win'
        if state[6]==player and state[7]==player and state[8]==player:
            return 'win'
        if state[0]==player and state[3]==player and state[6]==player:
            return 'win'
        if state[1]==player and state[4]==player and state[7]==player:
            return 'win'
        if state[3]==player and state[5]==player and state[8]==player:
            return 'win'
        if state[0]==player and state[4]==player and state[8]==player:
            return 'win'
        if state[2]==player and state[4]==player and state[6]==player:
            return 'win'

        # checking for empty places
        for i in range(9):
            if state[i]==0:
                return

        return 'stalemate'

    def checkValid(self, state):
        validMoves=[]

        for i in range(9):
            if state[i]==0:
                validMoves.append(i)

        return validMoves

    def play(self):
        while True:
            print "Player ", self.player
            while True:
                try:
                    move = input("What is your move? ")

                    #check valid
                    validMoves = self.checkValid(self.state)

                    if move in validMoves:
                        break
                    else:
                        print "Illegal move!"
                except:
                    print "Write number from 0-8!"

            self.state[move] = self.player
            self.stateObj.displayState()

            winStatus = self.checkWin(self.state, self.player)
            if winStatus == 'win':
                print "Player ", self.player, " wins!"
                break
            if winStatus == 'loses':
                print "Player ", self.player, " loses!"
                break
            if winStatus == 'stalemate':
                print "Stalemate!"
                break

            if self.player == 1:
                self.player = 2
            else:
                self.player = 1

        print "Game Over"

if __name__ == '__main__':
    tictactoe = TicTacToe()
    tictactoe.play()
